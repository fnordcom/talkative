from gi.repository import Gtk

class Window(Gtk.ApplicationWindow):
	def __init__(self, **kw):
		super().__init__(default_width=800, default_height=600, **kw)

		self._init_window()
		self._init_roster_pane()

	def _init_window(self):
		self.main_builder = Gtk.Builder()
		self.main_builder.add_from_resource('/de/fnordcom/talkative/ui/window.ui')

		titlebar = self.main_builder.get_object('main-header')
		self.set_titlebar(titlebar)

		main_grid = self.main_builder.get_object('main-grid')
		self.add(main_grid)

	def _init_roster_pane(self):
		builder = Gtk.Builder()
		builder.add_from_resource('/de/fnordcom/talkative/ui/roster.ui')

		roster_pane = self.main_builder.get_object('roster-pane')
		roster_pane_content = builder.get_object('roster-pane__content')

		roster_pane.add(roster_pane_content)

		# Add some fake chats
		chat_list = builder.get_object('roster-pane__chat-list')
		self._add_to_roster('Geheimorganisation Geheimorganisation', chat_list)
		self._add_to_roster('Hans Dampf', chat_list)
		self._add_to_roster('Maja Muster', chat_list)

	def _add_to_roster(self, name, chat_list):
		builder = Gtk.Builder()
		builder.add_from_resource('/de/fnordcom/talkative/ui/roster-item.ui')

		nameLabel = builder.get_object('chat-list__item__name')
		nameLabel.set_text(name)

		lastMessageLabel = builder.get_object('chat-list__item__last-message')
		lastMessageLabel.set_text('Foobar baz')

		lastMessageTimeLabel = builder.get_object('chat-list__item__last-message-time')
		lastMessageTimeLabel.set_text('15:25')

		list_item = builder.get_object('chat-list__item')
		chat_list.add(list_item)
