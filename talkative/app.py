from gi.repository import Gtk, Gdk, Gio

from talkative.window import Window

class App(Gtk.Application):
	def __init__(self):
		super().__init__(application_id='de.fnordcom.talkative')

	def _on_quit(self, action, parameter):
		self.quit()

	def _init_stylesheet(self):
		css = Gtk.CssProvider()
		css.load_from_resource('/de/fnordcom/talkative/style.css')

		screen = Gdk.Screen.get_default()
		Gtk.StyleContext.add_provider_for_screen(screen, css, Gtk.STYLE_PROVIDER_PRIORITY_USER)

	def _init_app_menu(self):
		builder = Gtk.Builder()
		builder.add_from_resource('/de/fnordcom/talkative/ui/app-menu.ui')

		aboutAction = Gio.SimpleAction.new('about')
		self.add_action(aboutAction)

		quitAction = Gio.SimpleAction.new('quit')
		quitAction.connect('activate', self._on_quit)
		self.set_accels_for_action('app.quit', ['<Control>q'])
		self.add_action(quitAction)

		menu = builder.get_object('app-menu')
		self.set_app_menu(menu)

	def do_activate(self):
		window = Window(application=self)
		window.show_all()

	def do_startup(self):
		Gtk.Application.do_startup(self)

		self._init_stylesheet()
		self._init_app_menu()
