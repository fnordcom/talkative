#!/usr/bin/env python
import os
import signal

from gi.repository import Gio
from talkative.app import App

if __name__ == "__main__":
    resource = Gio.Resource.load(os.path.join('data', 'talkative.gresource'))
    resource._register()

    app = App()
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    app.run()
