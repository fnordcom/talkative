# Talkative
Desktop instant messenger for the 21st century.

## Planned features
* Deep Gnome 3 integration
 * Modern GTK 3 based UI
 * [Gnome Online Accounts](https://developer.gnome.org/goa/stable/)
 * [Chat inside desktop notifications](https://developer.gnome.org/notification-spec/)
 * Contacts synchronised via `evolution-data-server`
* Superior XMPP support by including at least the following XEPs:
 * XEP-0198: Stream management
 * XEP-0280: Carbons
 * XEP-0313: Messages archive
 * XEP-0065: SOCKS5 Bytestreams
* All in all: A desktop pendant for the wonderful Android [Conversations](https://github.com/siacs/Conversations) app!

## Setup
1. Go into `data/` and run `make`
2. Run `./app`

## Development
ProTip™: Use <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>I</kbd> to open GtkInspector!

* Python 3
* Please note that there is a [.editorconfig](http://editorconfig.org/) file!
* We should try to separate the UI logic as much from the XMPP-stuff as possible
* [SleekXMPP](https://github.com/fritzy/SleekXMPP) should be a solid, modularised base
* Do not forget to modify the AUTHORS file
